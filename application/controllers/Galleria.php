<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galleria extends CI_Controller {
	public function __construct(){
            parent::__construct();  
	}
    
        public function index($kansio=''){
            $kansio=urldecode($kansio);
            
            //haetaan kaikki kansiot
            $data["kansiot"]=$this->kansio_model->hae_kaikki();
            
            if(strlen($kansio)===0 && count($data['kansiot'])>0) {
                $kansio=$data['kansiot'][0];
                $kansio=substr($kansio, 0, strlen($kansio)-1);
            }
            
            $data['valittu']=$kansio;
            $this->session->set_userdata('kansio',$kansio);
            
            //asetetaan sivupalkki ja sisältönäkymät data-muuttujaan
            $data["sivupalkki"]="kansio/kansiot_view";
            $data["sisalto"]="kuva/kuvat_view";
            //Näytetään template ja viedään tarvittavat tiedot näkymään
            $this->load->view("template.php",$data);
        }
        
        public function lisaa_kansio(){
            //asetetaan oletusarvot (tyhjät) lomakkeen muuttujiin
            $data=array(
                'nimi' => ''
            );
            
            //haetaan näkymissä tarvittavat tiedot data-muuttujaan
            $data["kansiot"]=$this->kansio_model->hae_kaikki();
            $data["valittu"]=$this->session->userdata("kansio");
            $data["sivupalkki"]="kansio/kansiot_view";
            $data["sisalto"]="kansio/kansio_view";
            
            //näytetään template ja viedään tarvittavat tiedot näkymiin data-muuttujassa (taulukko)
            $this->load->view("template.php",$data);
        }
        public function tallenna_kansio(){
            $kansio=$this->input->post("nimi");
            //lisätään uusi kansio ja luetana lisätyn kansion id paluuarvona muuttujaan
            $this->kansio_model->lisaa($kansio);
            //uudelleen ohjataan käyttäjä kansioiden selaussivulle ja välitetään juuri lisätyn kansion id parametrina (jolloin se tulee valituksi)
            redirect("galleria/index/$kansio");
        }
        
        public function poista_kansio(){
            //istuntomuuttuja sisältää valitun kansion
            $kansio=$this->session->userdata("kansio");
            //poistetaan kansio
            $this->kansio_model->poista($kansio);
            //poistetaan valitun kansion id istuntomuuttujasta, koska kansio juuri poistettiin
            $this->session->unset_userdata("kansio");
            //uudelleenohjataan käyttäjä kansioiden selaussivulle, jolloin haetaan 1. kansio valituksi(mikäli on kansioita)
            redirect("galleria/index");
        }
        
        public function lisaa_kuva(){
            //haetaan näkymissä tarvittavat tiedot data-muuttujaan
            $data["kansiot"]=$this->kansio_model->hae_kaikki();
            $data["valittu"]=$this->session->userdata("kansio");
            $data["sivupalkki"]="kansio/kansiot_view";
            $data["sisalto"]="kuva/kuvat_view";
            
            //näytetään template ja viedään tarvittavat tiedot näkymiin data-muuttujassa (taulukko)
            $this->load->view("template.php",$data);
        }
        
        public function lataa_kuva() {
            //asetetaan konfiguraatio upload-kirjastolle
            $kansio=$this->session->userdata("kansio");
            //$polku=$this->config->item("upload_path) . $kansio . "/";  miksi kommenttina?
            $this->kuva_model->lisaa($kansio);
            redirect("galleria/index/$kansio");
        }
}
