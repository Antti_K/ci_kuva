<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kansio_Model extends CI_Model {
	public function __construct(){
            parent::__construct();  
            $this->load->helper('directory');
	}
    public function hae_kaikki() {
        return array_keys(directory_map($this->config->item("upload_path")));
    }
    public function lisaa($kansio) {
        //if lauseeseen piti lisätä . '/' . jotta se toimi, muutoin se loi kansioita kuten uploadstestt
        if (!mkdir($this->config->item('upload_path') . '/' . $kansio)) {
            throw new Exception("Virhe luotaessa kansiota. Kansion luonti ei onnistunut.");
        }        
    }
    public function poista($kansio){
        //edellisen metodin lisäys tehty jo valmiiksi tänne..
        if (!rmdir($this->config->item('upload_path') . '/' . $kansio)) {
            throw new Exception("Kansion poisto ei onnistunut. Tarkasta, ettei kansiossa ole kuvia.");
        }    
    }
}