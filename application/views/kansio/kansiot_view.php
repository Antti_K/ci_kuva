
<h3>Kansiot sivu</h3>

<?php
foreach ($kansiot as $kansio){
    $ominaisuudet="";
    $kuva='img/kansio_kiinni.PNG';
    
    //poistetaan kauttaviiva kansion nimen lopusta
    $kansio=substr($kansio, 0, strlen($kansio)-1);
    
    if($kansio==$valittu) {
        $kuva='img/kansio_auki.PNG';
        $ominaisuudet=array('class' => 'valittu_kansio');
    }
    
    echo img($kuva);
    echo anchor('galleria/index/' . urlencode($kansio),$kansio,$ominaisuudet);
    echo br();
}


?>
